# NPCs Mod

The Lethal Company NPCs mod adds a variety of new creatures that spicy up your adventure to become the greatest asset for your employer.
Only one type of NPC is supported at the moment, hostiles, but it'd be cool to add neutral or allied npcs.

## Hostiles

## The Clone

A shapeshifting entity that mimics the appearance of crewmates.
It has been noted that the clone can mimic certain behaviours that other great assests exhibit.
After the loss of some of the greatest assets the company has ever known it is now recorded that it can be scared off by sussing it out (point at it).
