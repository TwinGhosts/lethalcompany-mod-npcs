﻿namespace Lethal.Company.Mod.Npcs.Static
{
    internal static class ModInfo
    {
        public const string GUID = "TwinGhosts.LethalCompany.Mod.Npcs";
        public const string NAME = "Lethal Company Npcs";
        public const string VERSION = "0.0.1";
    }
}
