namespace Lethal.Company.Mod.Npcs
{
    public static class PluginInfo
    {
        public const string PLUGIN_GUID = "Lethal Comapny NPC Mod";
        public const string PLUGIN_NAME = "Lethal Comapny NPC Mod";
        public const string PLUGIN_VERSION = "1.0.0";
    }
}
