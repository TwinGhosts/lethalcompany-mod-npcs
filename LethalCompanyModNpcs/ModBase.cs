﻿using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using Lethal.Company.Mod.Npcs.Static;

namespace Lethal.Company.Mod.Npcs
{
    [BepInPlugin(ModInfo.GUID, ModInfo.NAME, ModInfo.VERSION)]
    public class ModBase : BaseUnityPlugin
    {
        private readonly Harmony harmonyInstance = new(ModInfo.GUID);
        private ManualLogSource manualLogSource;

        private static ModBase Instance;

        private void Awake()
        {
            Instance ??= this;

            manualLogSource = BepInEx.Logging.Logger.CreateLogSource(ModInfo.GUID);
            manualLogSource.LogWarning($">>>>>>>>>>>> Plugin {ModInfo.GUID} | {ModInfo.NAME} | {ModInfo.VERSION} is loaded!");

            harmonyInstance.PatchAll();
        }
    }
}